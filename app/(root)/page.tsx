import { getCurrentUser } from "@/lib/actions/user.actions";
import { redirect } from "next/navigation";

export default async function Home() {
  const currentUser = await getCurrentUser();

  if (!currentUser?.onboarded) redirect("/onboarding");

  return <main>Home</main>;
}
