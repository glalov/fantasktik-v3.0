import FriendsTable from "@/components/dataTables/FriendsTable";
import {
  getAllUsers,
  getCurrentUserWithFullData,
} from "@/lib/actions/user.actions";
import { redirect } from "next/navigation";

const Page = async () => {
  const currentUser = await getCurrentUserWithFullData();
  if (!currentUser) return null;
  if (!currentUser?.onboarded) redirect("/onboarding");

  const allUsers = await getAllUsers();
  const potentialFriends = allUsers.filter(
    (user) =>
      !currentUser.friends.some((friend) => friend.id === user.id) &&
      !currentUser.friendOf.some((friend) => friend.id === user.id) &&
      user.id !== currentUser.id &&
      user.onboarded
  );

  return (
    <FriendsTable
      currentUser={currentUser}
      potentialFriends={potentialFriends}
    />
  );
};

export default Page;
