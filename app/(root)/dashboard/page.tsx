import { getCurrentUser } from "@/lib/actions/user.actions";
import { redirect } from "next/navigation";

const Page = async () => {
  const currentUser = await getCurrentUser();
  if (!currentUser) return null;
  if (!currentUser?.onboarded) redirect("/onboarding");

  return <div>Dashboard</div>;
};

export default Page;
