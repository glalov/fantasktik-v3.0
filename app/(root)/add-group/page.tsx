import AddGroupForm from "@/components/forms/AddGroupForm";
import { getCurrentUser } from "@/lib/actions/user.actions";
import { redirect } from "next/navigation";

const page = async () => {
  const currentUser = await getCurrentUser();
  if (!currentUser) return null;
  if (!currentUser?.onboarded) redirect("/onboarding");

  return <AddGroupForm currentUser={currentUser} />;
};

export default page;
