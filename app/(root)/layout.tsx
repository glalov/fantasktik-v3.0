import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "../globals.css";
import { getServerSession } from "next-auth";
import SessionProvider from "@/providers/SessionProvider";
import { authOptions } from "../api/auth/[...nextauth]/route";
import TopBar from "@/components/shared/TopBar";
import ClientOnly from "@/components/ClientOnly";
import { Toaster } from "@/components/ui/toaster";
import LeftSideBar from "@/components/shared/LeftSideBar";
import BottomBar from "@/components/shared/BottomBar";
import { getCurrentUser } from "@/lib/actions/user.actions";
import { Providers } from "../providers";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "FanTaskTik",
  description: "Task Management app",
};

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  //const session = await getServerSession(authOptions);
  const currentUser = await getCurrentUser();
  if (!currentUser) return null;
  return (
    <html lang="en">
      <body className={inter.className}>
        <Providers>
          <TopBar currentUser={currentUser} />
          <main className="flex flex-row">
            <LeftSideBar />
            <section className="main-container">
              <div className="w-full max-w-4xl">{children}</div>
            </section>
          </main>

          <BottomBar />
          <Toaster />
        </Providers>
      </body>
    </html>
  );
}
