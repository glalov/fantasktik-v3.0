import AccountProfile from "@/components/forms/AccountProfile";
import { getCurrentUser } from "@/lib/actions/user.actions";
import { redirect } from "next/navigation";

const page = async () => {
  const currentUser = await getCurrentUser();

  if (!currentUser) return null;
  if (!currentUser.onboarded) return redirect("/onboarding");

  return <AccountProfile currentUser={currentUser} btnTitle="Update Profile" />;
};

export default page;
