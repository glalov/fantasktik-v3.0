import ProfileCard from "@/components/cards/ProfileCard";
import { getCurrentUserWithFullData } from "@/lib/actions/user.actions";
import { redirect } from "next/navigation";

const Page = async () => {
  const currentUser = await getCurrentUserWithFullData();

  if (!currentUser) return null;
  if (!currentUser.onboarded) return redirect("/onboarding");

  return <ProfileCard user={currentUser} />;
};

export default Page;
