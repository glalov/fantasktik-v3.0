import EditTaskForm from "@/components/forms/EditTaskForm";
import { isUserInGroup } from "@/lib/actions/group.actions";
import { getTaskById } from "@/lib/actions/task.actions";
import { getCurrentUserWithFullData } from "@/lib/actions/user.actions";
import { redirect } from "next/navigation";
import React from "react";

const Page = async ({ params }: { params: { id: string } }) => {
  // Get current user
  const currentUser = await getCurrentUserWithFullData();
  if (!currentUser) return null;
  if (!currentUser?.onboarded) redirect("/onboarding");

  // Get current task
  const currentTask = await getTaskById(params.id);
  if (!currentTask) return <div>Task with this id is not found</div>;

  // Check if user is a member of this task's group
  const isMember = isUserInGroup(currentTask.group.id, currentUser.id);
  if (!isMember) return <div>You do not have access to this task</div>;

  return <EditTaskForm task={currentTask} currentUser={currentUser} />;
};

export default Page;
