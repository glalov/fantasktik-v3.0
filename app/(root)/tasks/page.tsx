import TasksTable from "@/components/dataTables/TasksTable";
import { Button } from "@/components/ui/button";
import { getCurrentUserWithFullData } from "@/lib/actions/user.actions";
import Link from "next/link";
import { redirect } from "next/navigation";

const page = async () => {
  const currentUser = await getCurrentUserWithFullData();
  if (!currentUser) return null;
  if (!currentUser?.onboarded) redirect("/onboarding");

  const allTasks = [...currentUser.createdTasks, ...currentUser.assignedTasks];

  if (allTasks.length === 0)
    return (
      <section>
        <Button>
          <Link href="/add-task">First create your first task</Link>
        </Button>
      </section>
    );

  return (
    <section>
      <h1>All Tasks</h1>
      <div>
        <Button>
          <Link href="/calendar">Calendar View</Link>
        </Button>
      </div>
      <TasksTable tasks={allTasks} currentUser={currentUser} />
    </section>
  );
};

export default page;
