import MembersTable from "@/components/dataTables/MembersTable";
import MembersHeader from "@/components/tableHeaders/MembersHeader";
import { getGroupById, isUserInGroup } from "@/lib/actions/group.actions";
import { getCurrentUserWithFullData } from "@/lib/actions/user.actions";
import { redirect } from "next/navigation";

const Page = async ({ params }: { params: { id: string } }) => {
  // Get current user
  const currentUser = await getCurrentUserWithFullData();
  if (!currentUser) return null;
  if (!currentUser?.onboarded) redirect("/onboarding");

  // Get current group
  const currentGroup = await getGroupById(params.id);
  if (!currentGroup) return <div>Group with this id is not found</div>;

  // Check if user is a member of this group
  const isMember = isUserInGroup(currentGroup.id, currentUser.id);
  if (!isMember) return <div>You do not have access to this group</div>;

  return (
    <>
      <MembersHeader group={currentGroup} />
      <MembersTable
        members={currentGroup.users}
        currentUser={currentUser}
        groupCreatorId={currentGroup.creatorID}
        groupID={currentGroup.id}
      />
    </>
  );
};

export default Page;
