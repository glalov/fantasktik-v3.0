import GroupsTable from "@/components/dataTables/GroupsTable";
import { Button } from "@/components/ui/button";
import { getCurrentUserWithFullData } from "@/lib/actions/user.actions";
import Link from "next/link";
import { redirect } from "next/navigation";

const page = async () => {
  const currentUser = await getCurrentUserWithFullData();
  if (!currentUser) return null;
  if (!currentUser?.onboarded) redirect("/onboarding");

  if (!currentUser.groups || currentUser.groups.length === 0)
    return (
      <section>
        <Button>
          <Link href="/add-group">First add your first group</Link>
        </Button>
      </section>
    );

  return (
    <div>
      <GroupsTable groups={currentUser.groups} currentUser={currentUser} />
    </div>
  );
};

export default page;
