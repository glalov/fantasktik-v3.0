import AllTasksCalendar from "@/components/calendar/AllTasksCalendar";
import { getCurrentUserWithFullData } from "@/lib/actions/user.actions";
import { redirect } from "next/navigation";

const Page = async () => {
  const currentUser = await getCurrentUserWithFullData();
  if (!currentUser) return null;
  if (!currentUser?.onboarded) redirect("/onboarding");

  return <AllTasksCalendar user={currentUser} />;
};

export default Page;
