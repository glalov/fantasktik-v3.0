import AccountProfile from "@/components/forms/AccountProfile";
import { getCurrentUser } from "@/lib/actions/user.actions";
import { redirect } from "next/navigation";

const Page = async () => {
  const currentUser = await getCurrentUser();
  if (!currentUser) {
    return null;
  }

  if (currentUser.onboarded) redirect("/dashboard");

  return (
    <main className="mx-auto flex max-w-3xl flex-col justify-start px-10 py-20">
      <h1 className="head-text">Onboarding</h1>
      <p className="mt-3 text-base-regular">
        Complete your profile now, to use FanTaskTik.
      </p>

      <section className="mt-9 p-10">
        <AccountProfile currentUser={currentUser} btnTitle="Continue" />
      </section>
    </main>
  );
};

export default Page;
