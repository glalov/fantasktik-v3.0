"use client";

import { ColumnDef } from "@tanstack/react-table";
import ToggleSortingButton from "../buttons/ToggleSortingButton";
import FriendsActionDropdown from "../actionDropdowns/FriendsActionDropdown";

export type FriendsColumn = {
  id: string;
  name: string;
  username: string;
  email: string;
};

export const FriendsColumns: ColumnDef<FriendsColumn>[] = [
  {
    header: ({ column }) => {
      return <ToggleSortingButton column={column} text="Name" />;
    },
    accessorKey: "name",
    enableHiding: false,
  },
  {
    header: ({ column }) => {
      return <ToggleSortingButton column={column} text="Username" />;
    },
    accessorKey: "username",
  },
  {
    header: ({ column }) => {
      return <ToggleSortingButton column={column} text="Email" />;
    },
    accessorKey: "email",
  },
  {
    id: "actions",
    cell: ({ row, table }) => {
      return (
        <FriendsActionDropdown
          friendsID={row.original.id}
          friendsName={row.original.name}
          deleteRow={() => table.options.meta?.deleteRow(row.index)}
        />
      );
    },
  },
];
