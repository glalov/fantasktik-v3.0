"use client";

import { ColumnDef } from "@tanstack/react-table";
import Link from "next/link";
import TaskDropdown from "../ui/TaskDropdown";
import { taskPriorities, taskStatuses } from "@/constants";
import {
  updateTaskPriority,
  updateTaskStatus,
} from "@/lib/actions/task.actions";
import { TaskPriority, TaskStatus } from "@prisma/client";
import ToggleSortingButton from "../buttons/ToggleSortingButton";
import TaskActionDropdown from "../actionDropdowns/TaskActionDropdown";

export type TaskColumn = {
  id: string;
  name: string;
  notes: string | null;
  status: string;
  priority: string;
  createdAt: Date;
  startDate: Date | null;
  dueDate: Date | null;
  assignedToID: string | null;
};

export const taskColumns: ColumnDef<TaskColumn>[] = [
  {
    header: ({ column }) => {
      return <ToggleSortingButton column={column} text="Name" />;
    },
    accessorKey: "name",
    cell: ({ row }) => {
      return (
        <Link href={`/tasks/${row.original.id}`}>{row.original.name}</Link>
      );
    },
    enableHiding: false,
  },
  // {
  //   header: "Notes",
  //   accessorKey: "notes",
  // },
  {
    header: ({ column }) => {
      return <ToggleSortingButton column={column} text="Status" />;
    },
    accessorKey: "status",
    cell: ({ row, column: { id: columnId }, table }) => {
      return (
        <TaskDropdown
          content={taskStatuses}
          onValueChange={(newStatus) => {
            if (newStatus !== row.original.status)
              updateTaskStatus(row.original.id, newStatus as TaskStatus);
            table.options.meta?.updateData(row.index, columnId, newStatus);
          }}
          defaultValue={row.original.status}
        />
      );
    },
    filterFn: (row, id, value) => {
      return value.includes(row.getValue(id));
    },
  },
  {
    header: ({ column }) => {
      return <ToggleSortingButton column={column} text="Priority" />;
    },
    accessorKey: "priority",
    cell: ({ row, column: { id: columnId }, table }) => {
      return (
        <TaskDropdown
          content={taskPriorities}
          onValueChange={(newPriority) => {
            if (newPriority !== row.original.priority) {
              updateTaskPriority(row.original.id, newPriority as TaskPriority);
              table.options.meta?.updateData(row.index, columnId, newPriority);
            }
          }}
          defaultValue={row.original.priority}
        />
      );
    },
    filterFn: (row, id, value) => {
      return value.includes(row.getValue(id));
    },
  },
  {
    header: ({ column }) => {
      return <ToggleSortingButton column={column} text="Created At" />;
    },
    accessorKey: "createdAt",
    cell: ({ row }) => {
      return new Date(row.original.createdAt).toLocaleDateString();
    },
  },
  {
    header: ({ column }) => {
      return <ToggleSortingButton column={column} text="Start Date" />;
    },
    accessorKey: "startDate",
    cell: ({ row }) => {
      if (!row.original.startDate) return "-";
      return new Date(row.original.startDate).toLocaleDateString();
    },
  },
  {
    header: ({ column }) => {
      return <ToggleSortingButton column={column} text="Due Date" />;
    },
    accessorKey: "dueDate",
    cell: ({ row }) => {
      if (!row.original.dueDate) return "-";
      return new Date(row.original.dueDate).toLocaleDateString();
    },
  },
  {
    id: "actions",
    cell: ({ row, table }) => {
      return (
        <TaskActionDropdown
          taskId={row.original.id}
          taskName={row.original.name}
          deleteRow={() => table.options.meta?.deleteRow(row.index)}
        />
      );
    },
  },
];
