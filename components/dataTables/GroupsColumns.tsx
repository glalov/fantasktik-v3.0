"use client";

import { ColumnDef } from "@tanstack/react-table";
import Link from "next/link";
import ToggleSortingButton from "../buttons/ToggleSortingButton";
import GroupActionDropdown from "../actionDropdowns/GroupActionDropdown";

export type GroupColumn = {
  id: string;
  name: string;
  description: string | null;
};

export const groupColumns: ColumnDef<GroupColumn>[] = [
  {
    header: ({ column }) => {
      return <ToggleSortingButton column={column} text="Name" />;
    },
    accessorKey: "name",
    cell: ({ row }) => {
      return (
        <Link href={`/groups/${row.original.id}`}>{row.original.name}</Link>
      );
    },
    enableHiding: false,
  },
  {
    header: "Description",
    accessorKey: "description",
  },
  {
    id: "actions",
    cell: ({ row, table }) => {
      return (
        <GroupActionDropdown
          groupID={row.original.id}
          groupName={row.original.name}
          deleteRow={() => table.options.meta?.deleteRow(row.index)}
        />
      );
    },
  },
];
