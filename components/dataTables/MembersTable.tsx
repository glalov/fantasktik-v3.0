import React from "react";
import { membersColumns } from "./MembersColumns";
import { DataTable } from "../ui/DataTable";
import { FullUserData } from "@/lib/actions/user.actions";
import { User } from "@prisma/client";
import ShortAddMemberForm from "../forms/ShortAddMemberForm";

type MembersTableProps = {
  members: User[];
  currentUser: FullUserData;
  groupCreatorId: string;
  groupID: string;
};

const MembersTable = ({
  members,
  currentUser,
  groupCreatorId,
  groupID,
}: MembersTableProps) => {
  const membersData = members.map((member) => ({
    id: member.id,
    name: member.name,
    username: member.username,
    email: member.email,
    isOwner: member.id === groupCreatorId,
    image: member.image,
  }));

  const potentialFriends = [
    ...currentUser.friends.filter(
      (friend) => !membersData.find((member) => member.id === friend.id)
    ),
    ...currentUser.friendOf.filter(
      (friend) => !membersData.find((member) => member.id === friend.id)
    ),
  ];

  return (
    <div className="mx-auto py-10">
      <DataTable
        columns={membersColumns}
        data={membersData}
        currentUser={currentUser}
        shortAddForm={ShortAddMemberForm}
        potentialFriends={potentialFriends}
        groupID={groupID}
      />
    </div>
  );
};

export default MembersTable;
