import { Group } from "@prisma/client";
import { DataTable } from "../ui/DataTable";
import { groupColumns } from "./GroupsColumns";
import { FullUserData } from "@/lib/actions/user.actions";
import ShortAddGroupForm from "@/components/forms/ShortAddGroupForm";

type GroupsTableProps = {
  groups: Group[];
  currentUser: FullUserData;
};

const GroupsTable = ({ groups, currentUser }: GroupsTableProps) => {
  return (
    <div className="mx-auto py-10">
      <DataTable
        columns={groupColumns}
        data={groups}
        currentUser={currentUser}
        shortAddForm={ShortAddGroupForm}
      />
    </div>
  );
};

export default GroupsTable;
