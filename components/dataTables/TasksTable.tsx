import { Task } from "@prisma/client";
import { DataTable } from "../ui/DataTable";
import { taskColumns } from "./TasksColumns";
import { taskFacetedFilters } from "@/constants";
import { FullUserData } from "@/lib/actions/user.actions";
import ShortAddTaskForm from "../forms/ShortAddTaskForm";

type TasksTableProps = {
  tasks: Task[];
  currentUser: FullUserData;
  groupID?: string;
};

const TasksTable = ({ tasks, currentUser, groupID }: TasksTableProps) => {
  return (
    <div className="mx-auto py-10">
      <DataTable
        columns={taskColumns}
        data={tasks}
        facetedFilters={taskFacetedFilters}
        shortAddForm={ShortAddTaskForm}
        currentUser={currentUser}
        groupID={groupID}
      />
    </div>
  );
};

export default TasksTable;
