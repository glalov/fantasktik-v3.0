import { User } from "@prisma/client";
import { DataTable } from "../ui/DataTable";
import { FriendsColumn, FriendsColumns } from "./FriendsColumns";
import { FullUserData } from "@/lib/actions/user.actions";
import ShortAddFriendForm from "../forms/ShortAddFriendForm";

type FriendsTableProps = {
  currentUser: FullUserData;
  potentialFriends: User[];
};

const FriendsTable = ({ currentUser, potentialFriends }: FriendsTableProps) => {
  const allFriends = [...currentUser.friends, ...currentUser.friendOf];

  return (
    <div className="mx-auto py-10">
      <DataTable
        columns={FriendsColumns}
        data={allFriends as FriendsColumn[]}
        currentUser={currentUser}
        potentialFriends={potentialFriends}
        shortAddForm={ShortAddFriendForm}
      />
    </div>
  );
};

export default FriendsTable;
