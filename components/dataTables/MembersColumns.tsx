"use client";

import { ColumnDef } from "@tanstack/react-table";
import ToggleSortingButton from "../buttons/ToggleSortingButton";
import MembersActionDropdown from "../actionDropdowns/MembersActionDropdown";
import { Avatar } from "@nextui-org/avatar";

export type MembersColumn = {
  id: string;
  name: string | null;
  username: string | null;
  email: string | null;
  isOwner: boolean;
  image: string | null;
};

export const membersColumns: ColumnDef<MembersColumn>[] = [
  {
    header: ({ column }) => {
      return <ToggleSortingButton column={column} text="Name" />;
    },
    accessorKey: "name",
    enableHiding: false,
    cell: ({ row }) => {
      return (
        <div className="flex items-center">
          <Avatar
            src={row.original.image || undefined}
            name={row.original.username || undefined}
          />
          <div className="ml-2">
            <div>{row.original.name}</div>
            <div className="text-tiny text-gray-500">
              {row.original.username} ({row.original.email})
            </div>
          </div>
        </div>
      );
    },
  },
  {
    header: ({ column }) => {
      return <ToggleSortingButton column={column} text="Role" />;
    },
    accessorKey: "isOwner",
    cell: ({ getValue }) => {
      return getValue() ? "Owner" : "Member";
    },
  },
  {
    id: "actions",
    cell: ({ row, table }) => {
      return (
        <MembersActionDropdown
          memberID={row.original.id}
          memberName={row.original.name}
          deleteRow={() => table.options.meta?.deleteRow(row.index)}
          isOwner={row.original.isOwner}
        />
      );
    },
  },
];
