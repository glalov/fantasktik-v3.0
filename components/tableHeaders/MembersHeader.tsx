import { FullGroupData } from "@/lib/actions/group.actions";
import { Button } from "../ui/button";
import Link from "next/link";

type MembersHeaderProps = {
  group: FullGroupData;
};

const MembersHeader = ({ group }: MembersHeaderProps) => {
  return (
    <div>
      <h1>Members of {group.name}</h1>
      <Button>
        <Link href={`/groups/${group.id}`}>Return to group table</Link>
      </Button>
    </div>
  );
};

export default MembersHeader;
