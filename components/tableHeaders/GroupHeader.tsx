import { FullGroupData } from "@/lib/actions/group.actions";
import Link from "next/link";
import { GrEdit } from "react-icons/gr";
import { Button } from "../ui/button";
import GroupMembersView from "../members/GroupMembersView";

type GroupHeaderProps = {
  group: FullGroupData;
};

const GroupHeader = ({ group }: GroupHeaderProps) => {
  return (
    <>
      <div>
        {group.name}
        <Link className="inline-block" href={`/groups/${group.id}/edit`}>
          <GrEdit />
        </Link>
      </div>
      <div>{group.description}</div>

      <Button>
        <Link href={`/groups/${group.id}/calendar`}>Calendar View</Link>
      </Button>

      <GroupMembersView members={group.users} groupId={group.id} />
    </>
  );
};

export default GroupHeader;
