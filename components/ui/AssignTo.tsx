"use client";

import { useEffect, useState } from "react";
import { Button } from "./button";
import {
  CommandDialog,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
  CommandList,
} from "./command";
import { MdClear } from "react-icons/md";
import { Avatar, AvatarFallback, AvatarImage } from "./avatar";

export type AssignToUser = {
  id: string;
  username: string;
  image: string;
  email: string;
};

type AssignToProps = {
  assignedId: string | undefined;
  setAssignedId: (id: string) => void;
  users: AssignToUser[];
};

const AssignTo = ({ assignedId, setAssignedId, users }: AssignToProps) => {
  const [open, setOpen] = useState(false);
  const [selectedUsername, setSelectedUsername] = useState("");

  useEffect(() => {
    setSelectedUsername(users.find((u) => u.id === assignedId)?.username || "");
  }, [assignedId, users]);

  if (users.length === 0) return null;

  return (
    <div>
      <Button
        type="button"
        variant="outline"
        onClick={() => setOpen((open) => !open)}
      >
        {selectedUsername ? selectedUsername : "Select user"}
      </Button>
      {assignedId && (
        <span className="cursor-pointer" onClick={() => setAssignedId("")}>
          <MdClear className="inline-block m-1" />
        </span>
      )}
      <CommandDialog open={open} onOpenChange={setOpen}>
        <CommandInput className="input" placeholder="Search Users" />
        <CommandList>
          <CommandEmpty>No results found.</CommandEmpty>
          <CommandGroup>
            {users.map((user) => (
              <CommandItem
                className="flex gap-2 items-center"
                key={user.id}
                value={user.id}
                onSelect={(value) => {
                  setSelectedUsername(
                    users.find((u) => u.id === value)?.username || ""
                  );
                  setAssignedId(value);
                  setOpen(false);
                }}
              >
                <Avatar className="flex-shrink-0">
                  <AvatarImage src={user.image} alt="Avatar" />
                  <AvatarFallback>{user.username.charAt(0)}</AvatarFallback>
                </Avatar>
                <div className="flex flex-col">
                  <span className="text-sm">{user.username}</span>
                  <span className="text-xs">{user.email}</span>
                </div>
              </CommandItem>
            ))}
          </CommandGroup>
        </CommandList>
      </CommandDialog>
    </div>
  );
};

export default AssignTo;
