import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "./select";

type TaskDropdownProps = {
  content: { label: string; value: string; color: string }[];
  onValueChange: (value: string) => void;
  defaultValue: string;
};

const TaskDropdown = ({
  content,
  onValueChange,
  defaultValue,
}: TaskDropdownProps) => {
  return (
    <Select onValueChange={onValueChange} defaultValue={defaultValue}>
      <SelectTrigger
        style={{
          backgroundColor: content.find((el) => el.value === defaultValue)
            ?.color,
        }}
        className="w-[180px]"
      >
        <SelectValue />
      </SelectTrigger>
      <SelectContent>
        {content.map((item) => (
          <SelectItem
            style={{ backgroundColor: item.color }}
            key={item.value}
            value={item.value}
          >
            {item.label}
          </SelectItem>
        ))}
      </SelectContent>
    </Select>
  );
};

export default TaskDropdown;
