import { MoreHorizontal } from "lucide-react";
import { AlertDialog, AlertDialogTrigger } from "../ui/alert-dialog";
import { Button } from "../ui/button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuTrigger,
} from "../ui/dropdown-menu";
import Link from "next/link";
import AlertDialogContentWrapper from "../buttons/DropdownItemWithDialog";
import { useToast } from "../ui/use-toast";
import { deleteTask } from "@/lib/actions/task.actions";

type TaskActionDropdownProps = {
  taskId: string;
  taskName: string;
  deleteRow: () => void;
};

const TaskActionDropdown = ({
  taskId,
  taskName,
  deleteRow,
}: TaskActionDropdownProps) => {
  const { toast } = useToast();

  const onDeleteTask = async () => {
    try {
      await deleteTask(taskId);
      toast({
        title: "Task deleted",
        description: `Task '${taskName}' was deleted successfully.`,
        variant: "success",
      });
      deleteRow();
    } catch (error: any) {
      toast({
        title: "Error",
        description: `Failed to delete task '${taskName}'. Error: ${error.message}`,
        variant: "destructive",
      });
    }
  };
  return (
    <AlertDialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant="ghost" className="h-8 w-8 p-0">
            <span className="sr-only">Open menu</span>
            <MoreHorizontal className="h-4 w-4" />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent align="end">
          <DropdownMenuLabel>Actions</DropdownMenuLabel>
          <DropdownMenuItem>
            <Link href={`/tasks/${taskId}`}>View task</Link>
          </DropdownMenuItem>
          <DropdownMenuItem>
            <Link href={`/tasks/${taskId}/edit`}>Edit task</Link>
          </DropdownMenuItem>
          <AlertDialogTrigger asChild>
            <DropdownMenuItem className="cursor-pointer">
              Delete task
            </DropdownMenuItem>
          </AlertDialogTrigger>
        </DropdownMenuContent>
      </DropdownMenu>
      <AlertDialogContentWrapper
        text={`Do you want to delete task '${taskName}'?`}
        onContinue={onDeleteTask}
      />
    </AlertDialog>
  );
};

export default TaskActionDropdown;
