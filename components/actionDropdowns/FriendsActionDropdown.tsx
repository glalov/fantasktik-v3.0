import { MoreHorizontal } from "lucide-react";
import { AlertDialog, AlertDialogTrigger } from "../ui/alert-dialog";
import { Button } from "../ui/button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuTrigger,
} from "../ui/dropdown-menu";
import AlertDialogContentWrapper from "../buttons/DropdownItemWithDialog";
import { useToast } from "../ui/use-toast";
import { getCurrentUser, removeFriend } from "@/lib/actions/user.actions";

type FriendsActionDropdownProps = {
  friendsID: string;
  friendsName: string;
  deleteRow: () => void;
};

const FriendsActionDropdown = ({
  friendsID,
  friendsName,
  deleteRow,
}: FriendsActionDropdownProps) => {
  const { toast } = useToast();

  const onDeleteFriends = async () => {
    try {
      deleteRow();
      toast({
        title: "Friend removed",
        description: `Friend '${friendsName}' was removed successfully.`,
        variant: "success",
      });

      const currentUser = await getCurrentUser();
      if (!currentUser) return;

      await removeFriend(currentUser?.id, friendsID);
    } catch (error: any) {
      toast({
        title: "Error",
        description: `Failed to delete friend '${friendsName}'. Error: ${error.message}`,
        variant: "destructive",
      });
    }
  };

  return (
    <AlertDialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant="ghost" className="h-8 w-8 p-0">
            <span className="sr-only">Open menu</span>
            <MoreHorizontal className="h-4 w-4" />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent align="end">
          <DropdownMenuLabel>Actions</DropdownMenuLabel>
          <DropdownMenuItem>View</DropdownMenuItem>
          <AlertDialogTrigger asChild>
            <DropdownMenuItem className="cursor-pointer">
              Remove
            </DropdownMenuItem>
          </AlertDialogTrigger>
        </DropdownMenuContent>
      </DropdownMenu>
      <AlertDialogContentWrapper
        text={`Do you want to remove '${friendsName}' from your friend's list?`}
        onContinue={onDeleteFriends}
      />
    </AlertDialog>
  );
};

export default FriendsActionDropdown;
