"use client";

import { MoreHorizontal } from "lucide-react";
import { AlertDialog, AlertDialogTrigger } from "../ui/alert-dialog";
import { Button } from "../ui/button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuTrigger,
} from "../ui/dropdown-menu";
import { useToast } from "../ui/use-toast";
import AlertDialogContentWrapper from "../buttons/DropdownItemWithDialog";
import { useParams } from "next/navigation";
import { removeMember } from "@/lib/actions/group.actions";

type MembersActionDropdownProps = {
  memberID: string;
  memberName: string | null;
  deleteRow: () => void;
  isOwner: boolean;
};

const MembersActionDropdown = ({
  memberID,
  memberName,
  deleteRow,
  isOwner,
}: MembersActionDropdownProps) => {
  const { toast } = useToast();
  const { id: groupID } = useParams();

  const onDeleteMember = async () => {
    try {
      deleteRow();
      toast({
        title: "Member removed",
        description: `Member '${memberName}' was removed successfully.`,
        variant: "success",
      });

      await removeMember(groupID as string, memberID);
    } catch (error: any) {
      toast({
        title: "Error",
        description: `Failed to delete member '${memberName}'. Error: ${error.message}`,
        variant: "destructive",
      });
    }
  };

  return (
    <AlertDialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant="ghost" className="h-8 w-8 p-0">
            <span className="sr-only">Open menu</span>
            <MoreHorizontal className="h-4 w-4" />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent align="end">
          <DropdownMenuLabel>Actions</DropdownMenuLabel>
          <AlertDialogTrigger disabled={isOwner} asChild>
            <DropdownMenuItem className="cursor-pointer">
              Remove
            </DropdownMenuItem>
          </AlertDialogTrigger>
        </DropdownMenuContent>
      </DropdownMenu>
      <AlertDialogContentWrapper
        text={`Do you want to remove '${memberName}' from this group?`}
        onContinue={onDeleteMember}
      />
    </AlertDialog>
  );
};

export default MembersActionDropdown;
