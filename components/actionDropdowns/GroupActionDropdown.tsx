import { MoreHorizontal } from "lucide-react";
import { AlertDialog, AlertDialogTrigger } from "../ui/alert-dialog";
import { Button } from "../ui/button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuTrigger,
} from "../ui/dropdown-menu";
import Link from "next/link";
import AlertDialogContentWrapper from "../buttons/DropdownItemWithDialog";
import { deleteGroup } from "@/lib/actions/group.actions";
import { useToast } from "../ui/use-toast";

type GroupActionDropdownProps = {
  groupID: string;
  groupName: string;
  deleteRow: () => void;
};

const GroupActionDropdown = ({
  groupID,
  groupName,
  deleteRow,
}: GroupActionDropdownProps) => {
  const { toast } = useToast();

  const onDeleteGroup = async () => {
    try {
      await deleteGroup(groupID);
      toast({
        title: "Group deleted",
        description: `Group '${groupName}' was deleted successfully.`,
        variant: "success",
      });
      deleteRow();
    } catch (error: any) {
      toast({
        title: "Error",
        description: `Failed to delete group '${groupName}'. Error: ${error.message}`,
        variant: "destructive",
      });
    }
  };

  return (
    <AlertDialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant="ghost" className="h-8 w-8 p-0">
            <span className="sr-only">Open menu</span>
            <MoreHorizontal className="h-4 w-4" />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent align="end">
          <DropdownMenuLabel>Actions</DropdownMenuLabel>
          <DropdownMenuItem>
            <Link href={`/groups/${groupID}`}>View group</Link>
          </DropdownMenuItem>
          <DropdownMenuItem>
            <Link href={`/groups/${groupID}/edit`}>Edit group</Link>
          </DropdownMenuItem>
          <AlertDialogTrigger asChild>
            <DropdownMenuItem className="cursor-pointer">
              Delete group
            </DropdownMenuItem>
          </AlertDialogTrigger>
        </DropdownMenuContent>
      </DropdownMenu>
      <AlertDialogContentWrapper
        text={`Do you want to delete group '${groupName}' and all of it's tasks?`}
        onContinue={onDeleteGroup}
      />
    </AlertDialog>
  );
};

export default GroupActionDropdown;
