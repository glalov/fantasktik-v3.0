"use client";

import { signOut } from "next-auth/react";
import { Button } from "../ui/button";
import { useRouter } from "next/navigation";

const SignOutBtn = () => {
  const router = useRouter();
  return (
    <Button
      onClick={() => {
        router.replace("/");
        signOut();
      }}
    >
      Sign Out
    </Button>
  );
};

export default SignOutBtn;
