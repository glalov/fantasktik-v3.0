import { ArrowUpDown } from "lucide-react";
import { Button } from "../ui/button";
import { Column } from "@tanstack/react-table";

type ToggleSortingButtonProps = {
  column: Column<any>;
  text: string;
};

const ToggleSortingButton = ({ column, text }: ToggleSortingButtonProps) => {
  return (
    <Button
      variant="ghost"
      onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
    >
      {text}
      <ArrowUpDown className="ml-2 h-4 w-4" />
    </Button>
  );
};

export default ToggleSortingButton;
