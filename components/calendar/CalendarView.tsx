"use client";

import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import { EventClickArg, EventMountArg } from "@fullcalendar/core/index.js";
import { useRouter } from "next/navigation";
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from "../ui/tooltip";

type CalendarEvent = {
  id: string;
  title: string;
  start: Date;
  end: Date | undefined;
  notes: string | null;
};

type CalendarViewProps = {
  events: CalendarEvent[];
};

const CalendarView = ({ events }: CalendarViewProps) => {
  const router = useRouter();

  const onClickEvent = (info: EventClickArg) => {
    router.push(`/tasks/${info.event.id}`);
  };

  const eventContent = (arg: EventMountArg) => {
    return (
      <div className="flex w-full items-center cursor-pointer overflow-hidden ml-1">
        <Tooltip>
          <TooltipTrigger>
            <div>{arg.event.title}</div>
          </TooltipTrigger>
          <TooltipContent>
            <div>{arg.event.title}</div>
            {arg.event.extendedProps.notes && (
              <div>{arg.event.extendedProps.notes}</div>
            )}
          </TooltipContent>
        </Tooltip>
      </div>
    );
  };

  return (
    <div>
      <TooltipProvider>
        <FullCalendar
          plugins={[dayGridPlugin]}
          initialView="dayGridMonth"
          headerToolbar={{
            left: "prev,next today",
            center: "title",
            right: "dayGridMonth,dayGridWeek,dayGridDay",
          }}
          events={events}
          eventClick={onClickEvent}
          eventContent={eventContent}
        />
      </TooltipProvider>
    </div>
  );
};

export default CalendarView;
