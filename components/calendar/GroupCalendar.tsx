import { FullGroupData } from "@/lib/actions/group.actions";
import CalendarView from "./CalendarView";

type GroupCalendarProps = {
  group: FullGroupData;
};

const GroupCalendar = ({ group }: GroupCalendarProps) => {
  const events = group.tasks.map((task) => ({
    id: task.id,
    title: task.name,
    start: task.startDate ? task.startDate : task.createdAt,
    end: task.dueDate || undefined,
    notes: task.notes,
  }));

  return <CalendarView events={events} />;
};

export default GroupCalendar;
