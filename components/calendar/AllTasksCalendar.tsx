import { FullUserData } from "@/lib/actions/user.actions";
import CalendarView from "./CalendarView";

type AllTasksCalendarProps = {
  user: FullUserData;
};

const AllTasksCalendar = ({ user }: AllTasksCalendarProps) => {
  const events = [...user.createdTasks, ...user.assignedTasks].map((task) => ({
    id: task.id,
    title: task.name,
    start: task.startDate ? task.startDate : task.createdAt,
    end: task.dueDate || undefined,
    notes: task.notes,
  }));
  return <CalendarView events={events} />;
};

export default AllTasksCalendar;
