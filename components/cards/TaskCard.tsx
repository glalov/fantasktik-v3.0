import { FullTaskData } from "@/lib/actions/task.actions";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "../ui/card";
import Link from "next/link";
import { GrEdit } from "react-icons/gr";
import { Button } from "../ui/button";

type TaskCardProps = {
  task: FullTaskData;
};

const TaskCard = ({ task }: TaskCardProps) => {
  return (
    <Card>
      <CardHeader>
        <CardTitle>
          {task.name}
          <Link className="inline-block" href={`/tasks/${task.id}/edit`}>
            <GrEdit />
          </Link>
        </CardTitle>
        {task.notes && <CardDescription>{task.notes}</CardDescription>}
      </CardHeader>
      <CardContent>
        <div>
          <div>Group: {task.group.name}</div>
          <div>Created at: {task.createdAt.toDateString()}</div>
          <div>Start date: {task.startDate?.toDateString()}</div>
          <div>Due date: {task.dueDate?.toDateString()}</div>
          <div>Priority: {task.priority}</div>
          <div>Status: {task.status}</div>
          <div>Assign To: {task.assignedTo?.username}</div>
        </div>
      </CardContent>
      <CardFooter>
        <Button>
          <Link href={`/groups/${task.groupID}`}>View Group</Link>
        </Button>
      </CardFooter>
    </Card>
  );
};

export default TaskCard;
