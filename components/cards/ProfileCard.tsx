import { FullUserData } from "@/lib/actions/user.actions";
import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle,
} from "../ui/card";
import Link from "next/link";
import { GrEdit } from "react-icons/gr";

type ProfileCardProps = {
  user: FullUserData;
};

const ProfileCard = ({ user }: ProfileCardProps) => {
  return (
    <Card>
      <CardHeader>
        <CardTitle>
          Profile
          <Link className="inline-block" href={`/profile/edit`}>
            <GrEdit />
          </Link>
        </CardTitle>
      </CardHeader>
      <CardContent>
        <div>name: {user.name}</div>
        <div>username: {user.username}</div>
        <div>email: {user.email}</div>
      </CardContent>
      <CardFooter></CardFooter>
    </Card>
  );
};

export default ProfileCard;
