import { User } from "@prisma/client";
import Image from "next/image";
import Link from "next/link";
import UserMenu from "./UserMenu";

type TopBarProps = {
  currentUser: User;
};

const TopBar = ({ currentUser }: TopBarProps) => {
  return (
    <nav className="topbar">
      <Link href="/dashboard" className="flex items-center gap-4">
        <Image
          src="/assets/Task_Manager_App_Logo.svg"
          alt="logo"
          width={28}
          height={28}
        />
        <p className="text-heading3-bold text-light-1 max-xs:hidden">
          FanTaskTik
        </p>
      </Link>
      <div className="flex items-center gap-1">
        <div className="block">
          <UserMenu
            userImage={currentUser.image || ""}
            userName={currentUser.name || ""}
          />
        </div>
      </div>
    </nav>
  );
};

export default TopBar;
