import { User } from "@prisma/client";
import { Avatar, AvatarGroup } from "@nextui-org/avatar";
import { Tooltip } from "@nextui-org/tooltip";
import { Button } from "../ui/button";
import Link from "next/link";

type GroupMembersViewProps = {
  members: User[];
  groupId: string;
};

const GroupMembersView = ({ members, groupId }: GroupMembersViewProps) => {
  return (
    <div>
      <span>Members:</span>
      <AvatarGroup isBordered max={5}>
        {members.map((member) => (
          <Tooltip
            key={member.id}
            content={
              <div className="px-1 py-2">
                <div className="text-small font-bold">{member.name}</div>
                <div className="text-tiny">
                  {member.username} ({member.email})
                </div>
              </div>
            }
            placement="bottom"
          >
            <Avatar
              src={member.image || undefined}
              name={member.username || undefined}
            />
          </Tooltip>
        ))}
      </AvatarGroup>
      <Button type="button">
        <Link href={`/groups/${groupId}/members`}>Manage Members</Link>
      </Button>
    </div>
  );
};

export default GroupMembersView;
