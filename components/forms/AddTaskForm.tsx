"use client";

import * as z from "zod";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { Button } from "../ui/button";
import { useRouter } from "next/navigation";
import { useToast } from "../ui/use-toast";
import { CreateTaskValidation } from "@/lib/validations/task";
import { FullUserData } from "@/lib/actions/user.actions";
import { checkUniqueName, createTask } from "@/lib/actions/task.actions";
import TaskForm from "./TaskForm";
import { useState } from "react";

type AddTaskFormProps = {
  currentUser: FullUserData;
};

const AddTaskForm = ({ currentUser }: AddTaskFormProps) => {
  const router = useRouter();
  const form = useForm<z.infer<typeof CreateTaskValidation>>({
    resolver: zodResolver(CreateTaskValidation),
    defaultValues: {
      name: "",
      notes: "",
      groupID: "",
      status: undefined,
      priority: undefined,
      rating: undefined,
      reminder: undefined,
      dateRange: undefined,
      assignedToID: undefined,
    },
  });
  const { toast } = useToast();
  const [disabled, setDisabled] = useState(false);

  // TO DO: Add rating, reminder, assignedToID

  const onSubmit = async (values: z.infer<typeof CreateTaskValidation>) => {
    // console.log(values);

    setDisabled(true);

    try {
      // Check if task name is already taken in the group
      const isUniqueName = await checkUniqueName(values.name, values.groupID);

      if (!isUniqueName) {
        form.setError("name", {
          type: "manual",
          message: "Task name already exists in this group",
        });
        setDisabled(false);
        return;
      }

      const newTask = await createTask({
        ...values,
        startDate: values.dateRange?.from,
        dueDate: values.dateRange?.to,
        creatorID: currentUser.id,
      });

      toast({
        variant: "success",
        title: "Task Added",
      });

      router.push(`/tasks/${newTask.id}`);
    } catch (error: any) {
      console.log(error);
      form.setError("root", {
        type: "manual",
        message: error.message,
      });
      setDisabled(false);
    }
  };

  return (
    <TaskForm
      form={form}
      onSubmit={onSubmit}
      currentUser={currentUser}
      hideAssignedTo={true}
    >
      <div className="buttons-wrapper">
        <Button className="button-item" disabled={disabled} type="submit">
          Create Task
        </Button>
      </div>
    </TaskForm>
  );
};

export default AddTaskForm;
