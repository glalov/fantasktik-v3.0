import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "../ui/form";
import { Input } from "../ui/input";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "../ui/select";
import { Textarea } from "../ui/textarea";
import * as z from "zod";
import { UseFormReturn } from "react-hook-form";
import { CreateTaskValidation } from "@/lib/validations/task";
import { taskPriorities, taskStatuses } from "@/constants";
import { FullUserData } from "@/lib/actions/user.actions";
import { DatePickerWithRange } from "../ui/DatePickerWithRange";
import AssignTo, { AssignToUser } from "../ui/AssignTo";

type TaskFormProps = {
  form: UseFormReturn<z.infer<typeof CreateTaskValidation>>;
  onSubmit: (values: z.infer<typeof CreateTaskValidation>) => void;
  children: React.ReactNode;
  currentUser: FullUserData;
  hideGroup?: boolean;
  hideAssignedTo?: boolean;
};

const TaskForm = ({
  form,
  onSubmit,
  children,
  currentUser,
  hideGroup,
  hideAssignedTo,
}: TaskFormProps) => {
  const groupId = form.getValues("groupID");
  const users: AssignToUser[] = [
    ...currentUser.friends.filter((friend) =>
      friend.groupIDs.includes(groupId)
    ),
    currentUser,
  ].map((friend) => ({
    id: friend.id,
    username: friend.username as string,
    image: friend.image || "",
    email: friend.email as string,
  }));
  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="form">
        <FormField
          control={form.control}
          name="name"
          render={({ field }) => (
            <FormItem className="form-item">
              <FormLabel>Name</FormLabel>
              <FormControl>
                <Input className="input" type="text" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        {!hideGroup && (
          <FormField
            control={form.control}
            name="groupID"
            render={({ field }) => (
              <FormItem className="form-item">
                <FormLabel>Group</FormLabel>
                <Select
                  value={field.value}
                  onValueChange={field.onChange}
                  defaultValue={field.value}
                >
                  <FormControl>
                    <SelectTrigger className="input">
                      <SelectValue placeholder="Select Group" />
                    </SelectTrigger>
                  </FormControl>
                  <SelectContent>
                    {currentUser.groups.map((group) => (
                      <SelectItem key={group.id} value={group.id}>
                        {group.name}
                      </SelectItem>
                    ))}
                  </SelectContent>
                </Select>
                <FormMessage />
              </FormItem>
            )}
          />
        )}
        {!hideAssignedTo && users.length > 0 && (
          <FormField
            control={form.control}
            name="assignedToID"
            render={({ field }) => (
              <FormItem className="form-item">
                <FormLabel>Assign To</FormLabel>
                <AssignTo
                  assignedId={field.value}
                  setAssignedId={field.onChange}
                  users={users}
                />
                <FormMessage />
              </FormItem>
            )}
          />
        )}
        <FormField
          control={form.control}
          name="status"
          render={({ field }) => (
            <FormItem className="form-item">
              <FormLabel>Status</FormLabel>
              <Select
                value={field.value}
                onValueChange={field.onChange}
                defaultValue={field.value}
              >
                <FormControl>
                  <SelectTrigger className="input">
                    <SelectValue placeholder="Select Status" />
                  </SelectTrigger>
                </FormControl>
                <SelectContent>
                  {taskStatuses.map((status) => (
                    <SelectItem key={status.value} value={status.value}>
                      {status.label}
                    </SelectItem>
                  ))}
                </SelectContent>
              </Select>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="priority"
          render={({ field }) => (
            <FormItem className="form-item">
              <FormLabel>Priority</FormLabel>
              <Select
                value={field.value}
                onValueChange={field.onChange}
                defaultValue={field.value}
              >
                <FormControl>
                  <SelectTrigger className="input">
                    <SelectValue placeholder="Select Priority" />
                  </SelectTrigger>
                </FormControl>
                <SelectContent>
                  {taskPriorities.map((priority) => (
                    <SelectItem key={priority.value} value={priority.value}>
                      {priority.label}
                    </SelectItem>
                  ))}
                </SelectContent>
              </Select>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="dateRange"
          render={({ field }) => (
            <FormItem className="form-item">
              <FormLabel>Date Range</FormLabel>
              <DatePickerWithRange
                date={field.value}
                setDate={field.onChange}
              />
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="notes"
          render={({ field }) => (
            <FormItem className="form-item-full">
              <FormLabel>Notes</FormLabel>
              <FormControl>
                <Textarea placeholder="Add notes for your task" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        {children}
      </form>
    </Form>
  );
};

export default TaskForm;
