"use client";

import { CreateGroupValidation } from "@/lib/validations/group";
import { Group } from "@prisma/client";
import { useForm } from "react-hook-form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useToast } from "../ui/use-toast";
import { useRouter } from "next/navigation";
import GroupForm from "./GroupForm";
import { Button } from "../ui/button";
import { updateGroup } from "@/lib/actions/group.actions";
import { useState } from "react";
import { set } from "date-fns";

type EditGroupFormProps = {
  group: Group;
};

const EditGroupForm = ({ group }: EditGroupFormProps) => {
  const router = useRouter();
  const form = useForm<z.infer<typeof CreateGroupValidation>>({
    resolver: zodResolver(CreateGroupValidation),
    defaultValues: {
      name: group.name,
      description: group.description || "",
    },
  });
  const { toast } = useToast();
  const [isFormChanged, setIsFormChanged] = useState(false);
  const [disabled, setDisabled] = useState(false);

  const onSubmit = async (values: z.infer<typeof CreateGroupValidation>) => {
    // console.log(values);

    setDisabled(true);

    try {
      const updatedGroup = await updateGroup({
        ...values,
        groupId: group.id,
      });
      toast({
        variant: "success",
        title: "Group Updated",
      });
      //router.push(`/groups/${updatedGroup.id}`);
      router.back();
    } catch (error: any) {
      console.log(error);
      form.setError("root", {
        type: "manual",
        message: error.message,
      });
      setDisabled(false);
    }
  };

  form.watch((values) => {
    if (
      values.name !== group.name ||
      values.description !== group.description
    ) {
      setIsFormChanged(true);
    } else {
      setIsFormChanged(false);
    }
  });

  const onCancel = () => {
    setIsFormChanged(false);
    router.back();
  };

  const onReset = () => {
    setIsFormChanged(false);
    form.reset();
  };

  return (
    <GroupForm form={form} onSubmit={onSubmit}>
      <div className="buttons-wrapper">
        <Button type="button" onClick={onCancel}>
          Cancel
        </Button>
        <Button type="button" onClick={onReset}>
          Reset
        </Button>
        <Button type="submit" disabled={!isFormChanged || disabled}>
          Save
        </Button>
      </div>
    </GroupForm>
  );
};

export default EditGroupForm;
