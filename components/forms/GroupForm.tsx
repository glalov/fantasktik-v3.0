import { CreateGroupValidation } from "@/lib/validations/group";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "../ui/form";
import { Input } from "../ui/input";
import { Textarea } from "../ui/textarea";
import * as z from "zod";
import { UseFormReturn } from "react-hook-form";

type GroupFormProps = {
  form: UseFormReturn<z.infer<typeof CreateGroupValidation>>;
  onSubmit: (values: z.infer<typeof CreateGroupValidation>) => void;
  children: React.ReactNode;
};

const GroupForm = ({ form, onSubmit, children }: GroupFormProps) => {
  return (
    <Form {...form}>
      <form className="form" onSubmit={form.handleSubmit(onSubmit)}>
        <FormField
          control={form.control}
          name="name"
          render={({ field }) => (
            <FormItem className="form-item-full">
              <FormLabel>Name</FormLabel>
              <FormControl>
                <Input
                  type="text"
                  className="account-form_input no-focus"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="description"
          render={({ field }) => (
            <FormItem className="form-item-full">
              <FormLabel>Description</FormLabel>
              <FormControl>
                <Textarea placeholder="Tell us about your group" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        {children}
      </form>
    </Form>
  );
};

export default GroupForm;
