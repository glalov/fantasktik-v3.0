"use client";

import * as z from "zod";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "../ui/form";
import { Input } from "../ui/input";
import { Button } from "../ui/button";
import { useRouter } from "next/navigation";
import { useToast } from "../ui/use-toast";
import { CreateTaskValidation } from "@/lib/validations/task";
import { FullUserData } from "@/lib/actions/user.actions";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "../ui/select";
import { checkUniqueName, createTask } from "@/lib/actions/task.actions";
import { Task } from "@prisma/client";
import { useState } from "react";

type ShortAddTaskFormProps = {
  currentUser: FullUserData;
  addRow: (row: Task) => void;
  groupID?: string;
};

const ShortAddTaskForm = ({
  currentUser,
  groupID,
  addRow,
}: ShortAddTaskFormProps) => {
  const router = useRouter();
  const form = useForm<z.infer<typeof CreateTaskValidation>>({
    resolver: zodResolver(CreateTaskValidation),
    defaultValues: {
      name: "",
      groupID: groupID || "",
    },
  });
  const { toast } = useToast();
  const [disabled, setDisabled] = useState(false);

  const onSubmit = async (values: z.infer<typeof CreateTaskValidation>) => {
    // console.log(values);

    setDisabled(true);

    try {
      // Check if task name is already taken in the group
      const isUniqueName = await checkUniqueName(values.name, values.groupID);

      if (!isUniqueName) {
        form.setError("name", {
          type: "manual",
          message: "Task name already exists in this group",
        });
        setDisabled(false);
        return;
      }

      const newTask = await createTask({
        ...values,
        creatorID: currentUser.id,
      });

      form.reset();

      toast({
        variant: "success",
        title: "Task Added",
      });

      addRow(newTask);
    } catch (error: any) {
      console.log(error);
      form.setError("root", {
        type: "manual",
        message: error.message,
      });
    } finally {
      setDisabled(false);
    }
  };

  return (
    <Form {...form}>
      <form
        className="flex flex-row justify-start gap-6"
        onSubmit={form.handleSubmit(onSubmit)}
      >
        <FormField
          control={form.control}
          name="name"
          render={({ field }) => (
            <FormItem className="">
              <FormControl>
                <Input
                  type="text"
                  className="account-form_input no-focus"
                  placeholder="New Task Name"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        {!groupID && (
          <FormField
            control={form.control}
            name="groupID"
            render={({ field }) => (
              <FormItem className="">
                <Select
                  onValueChange={field.onChange}
                  defaultValue={field.value}
                >
                  <FormControl>
                    <SelectTrigger className="w-[180px]">
                      <SelectValue placeholder="Select Group" />
                    </SelectTrigger>
                  </FormControl>
                  <SelectContent>
                    {currentUser.groups.map((group) => (
                      <SelectItem key={group.id} value={group.id}>
                        {group.name}
                      </SelectItem>
                    ))}
                  </SelectContent>
                </Select>
                <FormMessage />
              </FormItem>
            )}
          />
        )}
        <Button disabled={disabled} type="submit">
          Add Task
        </Button>
      </form>
    </Form>
  );
};

export default ShortAddTaskForm;
