"use client";

import { FriendsColumn } from "../dataTables/FriendsColumns";
import { Button } from "../ui/button";
import {
  CommandDialog,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
  CommandList,
} from "../ui/command";
import { Avatar, AvatarFallback, AvatarImage } from "../ui/avatar";
import { useEffect, useState } from "react";
import { FullUserData, addFriend } from "@/lib/actions/user.actions";
import { User } from "@prisma/client";

type ShortAddFriendFormProps = {
  currentUser: FullUserData;
  addRow: (row: FriendsColumn) => void;
  potentialFriends?: User[];
};

const ShortAddFriendForm = ({
  currentUser,
  addRow,
  potentialFriends,
}: ShortAddFriendFormProps) => {
  const [open, setOpen] = useState(false);
  const [friends, setFriends] = useState<User[]>([]);

  useEffect(() => {
    if (potentialFriends) {
      setFriends(potentialFriends);
    }
  }, [potentialFriends]);

  const onFriendSelect = async (friendId: string) => {
    const friend = friends?.find((user) => user.id === friendId);
    if (!friend) return;

    addRow(friend as FriendsColumn);
    setFriends((friends) => friends.filter((user) => user.id !== friendId));
    setOpen(false);
    await addFriend(currentUser.id, friendId);
  };

  if (friends?.length === 0) return null;

  return (
    <>
      <Button
        type="button"
        variant="outline"
        onClick={() => setOpen((open) => !open)}
      >
        Add Friend
      </Button>
      <CommandDialog open={open} onOpenChange={setOpen}>
        <CommandInput placeholder="Search Users" />
        <CommandList>
          <CommandEmpty>No results found.</CommandEmpty>
          <CommandGroup>
            {friends?.map((user) => (
              <CommandItem
                className="flex gap-2 items-center"
                key={user.id}
                value={user.id}
                onSelect={onFriendSelect}
              >
                <Avatar className="flex-shrink-0">
                  <AvatarImage src={user.image as string} alt="Avatar" />
                  <AvatarFallback>{user.username?.charAt(0)}</AvatarFallback>
                </Avatar>
                <div className="flex flex-col">
                  <span className="text-sm">{user.username}</span>
                  <span className="text-xs">{user.email}</span>
                </div>
              </CommandItem>
            ))}
          </CommandGroup>
        </CommandList>
      </CommandDialog>
    </>
  );
};

export default ShortAddFriendForm;
