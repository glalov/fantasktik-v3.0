"use client";

import * as z from "zod";

import { zodResolver } from "@hookform/resolvers/zod";
import { Group, User } from "@prisma/client";
import { useForm } from "react-hook-form";
import { CreateGroupValidation } from "@/lib/validations/group";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "../ui/form";
import { Input } from "../ui/input";
import { Button } from "../ui/button";
import { createGroup } from "@/lib/actions/group.actions";
import { useToast } from "../ui/use-toast";
import { useState } from "react";

type ShortAddGroupFormProps = {
  currentUser: User;
  addRow: (row: Group) => void;
};

const ShortAddGroupForm = ({ currentUser, addRow }: ShortAddGroupFormProps) => {
  const form = useForm<z.infer<typeof CreateGroupValidation>>({
    resolver: zodResolver(CreateGroupValidation),
    defaultValues: {
      name: "",
    },
  });
  const { toast } = useToast();
  const [disabled, setDisabled] = useState(false);

  const onSubmit = async (values: z.infer<typeof CreateGroupValidation>) => {
    // console.log(values);

    setDisabled(true);

    try {
      // TO DO: Check if group name is already taken for the current user

      const newGroup = await createGroup({
        ...values,
        creatorId: currentUser.id,
      });

      form.reset();

      toast({
        variant: "success",
        title: "Group Added",
      });

      addRow(newGroup);
    } catch (error: any) {
      console.log(error);
      form.setError("root", {
        type: "manual",
        message: error.message,
      });
    } finally {
      setDisabled(false);
    }
  };

  return (
    <Form {...form}>
      <form
        className="flex flex-row justify-start gap-6"
        onSubmit={form.handleSubmit(onSubmit)}
      >
        <FormField
          control={form.control}
          name="name"
          render={({ field }) => (
            <FormItem className="">
              <FormControl>
                <Input
                  type="text"
                  className="account-form_input no-focus"
                  placeholder="New Group Name"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Button disabled={disabled} type="submit">
          Add Group
        </Button>
      </form>
    </Form>
  );
};

export default ShortAddGroupForm;
