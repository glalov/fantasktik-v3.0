"use client";

import * as z from "zod";

import { zodResolver } from "@hookform/resolvers/zod";
import { User } from "@prisma/client";
import { usePathname, useRouter } from "next/navigation";
import { ChangeEvent, useState } from "react";
import { useForm } from "react-hook-form";
import { isBase64Image } from "@/lib/utils";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "../ui/form";
import Image from "next/image";
import { Input } from "../ui/input";
import { Button } from "../ui/button";
import { useUploadThing } from "@/lib/uploadthing";
import { UpdateUserValidation } from "@/lib/validations/user";
import { getUserByUsername, updateUser } from "@/lib/actions/user.actions";

type AccountProfileProps = {
  currentUser: User;
  btnTitle: string;
};

const AccountProfile = ({ currentUser, btnTitle }: AccountProfileProps) => {
  const router = useRouter();
  const pathname = usePathname();
  const { startUpload } = useUploadThing("media");

  const [files, setFiles] = useState<File[]>([]);
  const [disabled, setDisabled] = useState(false);

  const form = useForm<z.infer<typeof UpdateUserValidation>>({
    resolver: zodResolver(UpdateUserValidation),
    defaultValues: {
      image: currentUser?.image ? currentUser.image : "",
      name: currentUser?.name ? currentUser.name : "",
      username: currentUser?.username ? currentUser.username : "",
    },
  });

  const onSubmit = async (values: z.infer<typeof UpdateUserValidation>) => {
    setDisabled(true);
    const user = await getUserByUsername(values.username);

    // Check if username is already taken
    if (user && user.id !== currentUser.id) {
      form.setError("username", {
        type: "manual",
        message: "Username is already taken",
      });
      setDisabled(false);
      return;
    }

    const blob = values.image;

    const hasImageChanged = isBase64Image(blob);
    if (hasImageChanged) {
      const imgRes = await startUpload(files);

      if (imgRes && imgRes[0].url) {
        values.image = imgRes[0].url;
      }
    }

    try {
      await updateUser({
        name: values.name,
        username: values.username,
        userId: currentUser.id,
        image: values.image,
      });
    } catch (error: any) {
      form.setError("root", {
        type: "manual",
        message: error.message,
      });
      setDisabled(false);
    }

    if (pathname === "/profile/edit") {
      router.back();
    } else {
      router.push("/dashboard");
    }
  };

  const handleImage = (
    e: ChangeEvent<HTMLInputElement>,
    fieldChange: (value: string) => void
  ) => {
    e.preventDefault();

    const fileReader = new FileReader();

    if (e.target.files && e.target.files.length > 0) {
      const file = e.target.files[0];
      setFiles(Array.from(e.target.files));

      if (!file.type.includes("image")) return;

      fileReader.onload = async (event) => {
        const imageDataUrl = event.target?.result?.toString() || "";
        fieldChange(imageDataUrl);
      };

      fileReader.readAsDataURL(file);
    }
  };

  return (
    <Form {...form}>
      <form
        className="flex flex-col justify-start gap-10"
        onSubmit={form.handleSubmit(onSubmit)}
      >
        <FormField
          control={form.control}
          name="image"
          render={({ field }) => (
            <FormItem className="flex items-center gap-4">
              <FormLabel className="account-form_image-label">
                {field.value ? (
                  <Image
                    src={field.value}
                    alt="profile_icon"
                    width={96}
                    height={96}
                    priority
                    className="rounded-full object-contain"
                  />
                ) : (
                  <Image
                    src="/assets/profile.svg"
                    alt="profile_icon"
                    width={24}
                    height={24}
                    className="object-contain"
                  />
                )}
              </FormLabel>
              <FormControl className="flex-1 text-base-semibold text-gray-200">
                <Input
                  type="file"
                  accept="image/*"
                  placeholder="Add profile photo"
                  className="account-form_image-input"
                  onChange={(e) => handleImage(e, field.onChange)}
                />
              </FormControl>
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="name"
          render={({ field }) => (
            <FormItem className="flex w-full flex-col gap-3">
              <FormLabel className="text-base-semibold text-light-2">
                Name
              </FormLabel>
              <FormControl>
                <Input
                  type="text"
                  className="account-form_input no-focus"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="username"
          render={({ field }) => (
            <FormItem className="flex w-full flex-col gap-3">
              <FormLabel className="text-base-semibold text-light-2">
                Username
              </FormLabel>
              <FormControl>
                <Input
                  type="text"
                  className="account-form_input no-focus"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <Button disabled={disabled} type="submit">
          {btnTitle}
        </Button>
      </form>
    </Form>
  );
};

export default AccountProfile;
