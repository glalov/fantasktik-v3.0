"use client";

import * as z from "zod";

import { zodResolver } from "@hookform/resolvers/zod";
import { User } from "@prisma/client";
import { useForm } from "react-hook-form";
import { CreateGroupValidation } from "@/lib/validations/group";
import { Button } from "../ui/button";
import { createGroup } from "@/lib/actions/group.actions";
import { useRouter } from "next/navigation";
import { useToast } from "../ui/use-toast";
import GroupForm from "./GroupForm";
import { useState } from "react";
import { set } from "date-fns";

type AddGroupFormProps = {
  currentUser: User;
};

const AddGroupForm = ({ currentUser }: AddGroupFormProps) => {
  const router = useRouter();
  const form = useForm<z.infer<typeof CreateGroupValidation>>({
    resolver: zodResolver(CreateGroupValidation),
    defaultValues: {
      name: "",
      description: "",
    },
  });
  const { toast } = useToast();
  const [disabled, setDisabled] = useState(false);

  const onSubmit = async (values: z.infer<typeof CreateGroupValidation>) => {
    // console.log(values);

    setDisabled(true);

    try {
      // TO DO: Check if group name is already taken for the current user

      const newGroup = await createGroup({
        ...values,
        creatorId: currentUser.id,
      });

      toast({
        variant: "success",
        title: "Group Added",
      });

      router.push(`/groups/${newGroup.id}`);
    } catch (error: any) {
      console.log(error);
      form.setError("root", {
        type: "manual",
        message: error.message,
      });
      setDisabled(false);
    }
  };

  return (
    <GroupForm form={form} onSubmit={onSubmit}>
      <div className="buttons-wrapper">
        <Button disabled={disabled} type="submit">
          Create Group
        </Button>
      </div>
    </GroupForm>
  );
};

export default AddGroupForm;
