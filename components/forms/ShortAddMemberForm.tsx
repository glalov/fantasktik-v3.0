"use client";

import { useEffect, useState } from "react";
import { Button } from "../ui/button";
import {
  CommandDialog,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
  CommandList,
} from "../ui/command";
import { Avatar, AvatarFallback, AvatarImage } from "../ui/avatar";
import { User } from "@prisma/client";
import { MembersColumn } from "../dataTables/MembersColumns";
import { addMember } from "@/lib/actions/group.actions";

type ShortAddMemberFormProps = {
  groupID?: string;
  addRow: (row: MembersColumn) => void;
  potentialFriends?: User[];
};

const ShortAddMemberForm = ({
  addRow,
  groupID,
  potentialFriends,
}: ShortAddMemberFormProps) => {
  const [open, setOpen] = useState(false);
  const [friends, setFriends] = useState<User[]>([]);

  useEffect(() => {
    if (potentialFriends) {
      setFriends(potentialFriends);
    }
  }, [potentialFriends]);

  const onMemberSelect = async (memberId: string) => {
    if (!groupID) return;
    const friend = friends?.find((user) => user.id === memberId);
    if (!friend) return;

    addRow({ ...friend, isOwner: false } as MembersColumn);
    setFriends((friends) => friends.filter((user) => user.id !== memberId));
    setOpen(false);
    await addMember(groupID, memberId);
  };

  if (friends?.length === 0) return null;

  return (
    <>
      <Button
        type="button"
        variant="outline"
        onClick={() => setOpen((open) => !open)}
      >
        Add Member
      </Button>
      <CommandDialog open={open} onOpenChange={setOpen}>
        <CommandInput placeholder="Search Users from Friend List" />
        <CommandList>
          <CommandEmpty>No results found.</CommandEmpty>
          <CommandGroup>
            {friends?.map((user) => (
              <CommandItem
                className="flex gap-2 items-center"
                key={user.id}
                value={user.id}
                onSelect={onMemberSelect}
              >
                <Avatar className="flex-shrink-0">
                  <AvatarImage src={user.image as string} alt="Avatar" />
                  <AvatarFallback>{user.username?.charAt(0)}</AvatarFallback>
                </Avatar>
                <div className="flex flex-col">
                  <span className="text-sm">{user.username}</span>
                  <span className="text-xs">{user.email}</span>
                </div>
              </CommandItem>
            ))}
          </CommandGroup>
        </CommandList>
      </CommandDialog>
    </>
  );
};

export default ShortAddMemberForm;
