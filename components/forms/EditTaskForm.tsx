"use client";

import * as z from "zod";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { Button } from "../ui/button";
import { useRouter } from "next/navigation";
import { useToast } from "../ui/use-toast";
import { CreateTaskValidation } from "@/lib/validations/task";
import { FullUserData } from "@/lib/actions/user.actions";
import {
  FullTaskData,
  checkUniqueName,
  updateTask,
} from "@/lib/actions/task.actions";
import TaskForm from "./TaskForm";
import { useState } from "react";
import { compareProperties, getDateRange } from "@/lib/utils";
import { set } from "date-fns";

type EditTaskFormProps = {
  task: FullTaskData;
  currentUser: FullUserData;
};

const EditTaskForm = ({ task, currentUser }: EditTaskFormProps) => {
  const router = useRouter();
  const form = useForm<z.infer<typeof CreateTaskValidation>>({
    resolver: zodResolver(CreateTaskValidation),
    defaultValues: {
      name: task.name,
      notes: task.notes || "",
      groupID: task.groupID,
      status: task.status,
      priority: task.priority,
      rating: task.rating || undefined,
      reminder: task.reminder || undefined,
      dateRange: getDateRange(task.startDate, task.dueDate) || undefined,
      assignedToID: task.assignedToID || undefined,
    },
  });
  const { toast } = useToast();
  const [isFormChanged, setIsFormChanged] = useState(false);
  const [disabled, setDisabled] = useState(false);

  // TO DO: Add rating, reminder, assignedToID

  const onSubmit = async (values: z.infer<typeof CreateTaskValidation>) => {
    // console.log(values);

    setDisabled(true);

    try {
      // Check if task name is already taken in the group
      const isUniqueName = await checkUniqueName(
        values.name,
        values.groupID,
        task.id
      );

      if (!isUniqueName) {
        form.setError("name", {
          type: "manual",
          message: "Task name already exists in this group",
        });
        setDisabled(false);
        return;
      }

      const updatedTask = await updateTask({
        ...values,
        startDate: values.dateRange?.from,
        dueDate: values.dateRange?.to,
        taskId: task.id,
      });

      router.back();

      toast({
        variant: "success",
        title: "Task Updated",
      });
    } catch (error: any) {
      console.log(error);
      form.setError("root", {
        type: "manual",
        message: error.message,
      });
      setDisabled(false);
    }
  };

  form.watch((values) => {
    const compValues = {
      ...values,
      startDate: values.dateRange?.from,
      dueDate: values.dateRange?.to,
    };
    if (
      !compareProperties(compValues, task, [
        "name",
        "assignedToID",
        "notes",
        "groupID",
        "status",
        "priority",
        "rating",
        "reminder",
        "startDate",
        "dueDate",
      ])
    ) {
      setIsFormChanged(true);
    } else {
      setIsFormChanged(false);
    }
  });

  const onCancel = () => {
    setIsFormChanged(false);
    router.back();
  };

  const onReset = () => {
    setIsFormChanged(false);
    form.reset();
  };

  return (
    <TaskForm
      form={form}
      onSubmit={onSubmit}
      currentUser={currentUser}
      hideGroup={true}
    >
      <div className="buttons-wrapper">
        <Button className="button-item" type="button" onClick={onCancel}>
          Cancel
        </Button>
        <Button className="button-item" type="button" onClick={onReset}>
          Reset
        </Button>
        <Button
          className="button-item"
          type="submit"
          disabled={!isFormChanged || disabled}
        >
          Save
        </Button>
      </div>
    </TaskForm>
  );
};

export default EditTaskForm;
