import { type ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export function isBase64Image(imageData: string) {
  const base64Regex = /^data:image\/(png|jpe?g|gif|webp);base64,/;
  return base64Regex.test(imageData);
}

export function compareProperties(obj1: any, obj2: any, properties: string[]) {
  for (const property of properties) {
    const firstValue = obj1[property] || undefined;
    const secondValue = obj2[property] || undefined;
    if (firstValue !== secondValue) {
      return false;
    }
  }
  return true;
}

export function getDateRange(from: Date | null, to: Date | null) {
  if (!from) return null;
  const fromDate = new Date(from);
  const toDate = to ? new Date(to) : new Date(from);
  return { from: fromDate, to: toDate };
}
