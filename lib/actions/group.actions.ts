"use server";

import { Group, Task, User } from "@prisma/client";
import prisma from "@/lib/prismadb";
import { deleteUserGroup } from "./user.actions";
import { revalidatePath } from "next/cache";
import { unassignTask } from "./task.actions";

type CreateGroupParams = {
  name: string;
  description?: string;
  creatorId: string;
};

export async function createGroup({
  name,
  description,
  creatorId,
}: CreateGroupParams): Promise<Group> {
  try {
    const group = await prisma.group.create({
      data: {
        name,
        description,
        creator: {
          connect: {
            id: creatorId,
          },
        },
        users: {
          connect: {
            id: creatorId,
          },
        },
      },
      // include: {
      //   creator: true,
      //   users: true,
      // },
    });

    revalidatePath("/groups");

    return group;
  } catch (error: any) {
    throw new Error(`Failed to create group: ${error.message}`);
  }
}

export type FullGroupData = Group & {
  creator: User;
  users: User[];
  tasks: Task[];
};

export async function getGroupById(
  groupId: string
): Promise<FullGroupData | null> {
  try {
    const group = await prisma.group.findUnique({
      where: {
        id: groupId,
      },
      include: {
        creator: true,
        users: true,
        tasks: true,
      },
    });

    return group;
  } catch (error: any) {
    throw new Error(`Failed to get group: ${error.message}`);
  }
}

export async function isUserInGroup(
  groupId: string,
  userId: string
): Promise<boolean> {
  try {
    const group = await prisma.group.findUnique({
      where: {
        id: groupId,
      },
      include: {
        users: true,
      },
    });

    if (!group) {
      throw new Error(`Group not found`);
    }

    return group.users.some((user) => user.id === userId);
  } catch (error: any) {
    throw new Error(`Failed to get group: ${error.message}`);
  }
}

export async function deleteGroup(groupId: string): Promise<void> {
  try {
    const group = await getGroupById(groupId);

    group?.users.forEach(async (user) => {
      await deleteUserGroup(user.id, groupId);
    });

    await prisma.group.delete({
      where: {
        id: groupId,
      },
    });

    revalidatePath("/groups");
  } catch (error: any) {
    throw new Error(`Failed to delete group: ${error.message}`);
  }
}

type UpdateGroupParams = {
  groupId: string;
  name: string;
  description?: string;
};

export async function updateGroup({
  groupId,
  name,
  description,
}: UpdateGroupParams): Promise<Group> {
  try {
    const group = await prisma.group.update({
      where: {
        id: groupId,
      },
      data: {
        name,
        description,
      },
    });

    revalidatePath("/groups");
    revalidatePath(`/groups/${groupId}`);
    revalidatePath(`/groups/${groupId}/edit`);

    return group;
  } catch (error: any) {
    throw new Error(`Failed to update group: ${error.message}`);
  }
}

export async function removeMember(
  groupId: string,
  memberId: string
): Promise<void> {
  try {
    await prisma.group.update({
      where: {
        id: groupId,
      },
      data: {
        users: {
          disconnect: {
            id: memberId,
          },
        },
      },
    });

    const group = await getGroupById(groupId);

    if (!group) {
      throw new Error(`Group not found`);
    }

    // Unassign all tasks assigned to the member
    await Promise.all(
      group.tasks
        .filter((task) => task.assignedToID === memberId)
        .map(async (task) => {
          await unassignTask(task.id);
        })
    );

    revalidatePath(`/groups/${groupId}`);
    revalidatePath(`/groups/${groupId}/edit`);
    revalidatePath(`/groups/${groupId}/members`);
  } catch (error: any) {
    throw new Error(`Failed to remove member: ${error.message}`);
  }
}

export async function addMember(
  groupId: string,
  memberId: string
): Promise<void> {
  try {
    await prisma.group.update({
      where: {
        id: groupId,
      },
      data: {
        users: {
          connect: {
            id: memberId,
          },
        },
      },
    });

    revalidatePath(`/groups/${groupId}`);
    revalidatePath(`/groups/${groupId}/edit`);
    revalidatePath(`/groups/${groupId}/members`);
  } catch (error: any) {
    throw new Error(`Failed to add member: ${error.message}`);
  }
}
