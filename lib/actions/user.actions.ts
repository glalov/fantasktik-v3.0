"use server";

import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import prisma from "@/lib/prismadb";
import { User, Group, Task } from "@prisma/client";
import { getServerSession } from "next-auth";
import { revalidatePath } from "next/cache";

type CreateParams = {
  email: string;
  hashedPassword: string;
};

export async function createUser(createParams: CreateParams): Promise<void> {
  try {
    const user = await prisma.user.create({
      data: createParams,
    });
  } catch (error: any) {
    throw new Error(`Failed to create user: ${error.message}`);
  }
}

export async function getUserByEmail(email: string): Promise<User | null> {
  try {
    const user = await prisma.user.findUnique({
      where: {
        email,
      },
    });

    return user;
  } catch (error: any) {
    throw new Error(`Failed to find user by email: ${error.message}`);
  }
}

export async function getUserById(id: string): Promise<User | null> {
  try {
    const user = await prisma.user.findUnique({
      where: {
        id,
      },
    });

    return user;
  } catch (error: any) {
    throw new Error(`Failed to find user by id: ${error.message}`);
  }
}

export type FullUserData = User & {
  groups: Group[];
  createdGroups: Group[];
  createdTasks: Task[];
  assignedTasks: Task[];
  friends: User[];
  friendOf: User[];
};

export async function getFullUserDataByEmail(
  email: string
): Promise<FullUserData | null> {
  try {
    const user = await prisma.user.findUnique({
      where: {
        email,
      },
      include: {
        groups: true,
        createdGroups: true,
        createdTasks: true,
        assignedTasks: true,
        friends: true,
        friendOf: true,
      },
    });

    return user;
  } catch (error: any) {
    throw new Error(`Failed to find user by email: ${error.message}`);
  }
}

export async function getUserByUsername(
  username: string
): Promise<User | null> {
  try {
    const user = await prisma.user.findUnique({
      where: {
        username,
      },
    });

    return user;
  } catch (error: any) {
    throw new Error(`Failed to find user by username: ${error.message}`);
  }
}

export async function getCurrentUser(): Promise<User | null> {
  try {
    const session = await getServerSession(authOptions);

    if (!session?.user?.email) {
      return null;
    }

    const currentUser = await getUserByEmail(session.user.email);

    if (!currentUser) {
      return null;
    }

    return currentUser;
  } catch (error: any) {
    throw new Error(`Failed to find current user: ${error.message}`);
  }
}

export async function getCurrentUserWithFullData(): Promise<FullUserData | null> {
  try {
    const session = await getServerSession(authOptions);

    if (!session?.user?.email) {
      return null;
    }

    const currentUser = await getFullUserDataByEmail(session.user.email);

    if (!currentUser) {
      return null;
    }

    return currentUser;
  } catch (error: any) {
    throw new Error(`Failed to find current user: ${error.message}`);
  }
}

type UpdateUserParams = {
  userId: string;
  name: string;
  username: string;
  image: string;
};

export async function updateUser({
  userId,
  name,
  username,
  image,
}: UpdateUserParams): Promise<User | null> {
  try {
    const updatedUser = await prisma.user.update({
      where: {
        id: userId,
      },
      data: {
        name,
        username,
        image,
        onboarded: true,
      },
    });

    revalidatePath("/profile");
    revalidatePath("/profile/edit");

    return updatedUser;
  } catch (error: any) {
    throw new Error(`Failed to update user: ${error.message}`);
  }
}

export async function deleteUserGroup(
  userId: string,
  groupId: string
): Promise<void> {
  try {
    await prisma.user.update({
      where: { id: userId },
      data: {
        groups: {
          disconnect: {
            id: groupId,
          },
        },
      },
    });
  } catch (error: any) {
    throw new Error(`Failed to delete group from users: ${error.message}`);
  }
}

export async function getAllUsers(): Promise<User[]> {
  try {
    const users = await prisma.user.findMany();

    return users;
  } catch (error: any) {
    throw new Error(`Failed to find all users: ${error.message}`);
  }
}

export async function addFriend(
  userId: string,
  friendId: string
): Promise<void> {
  try {
    await prisma.user.update({
      where: { id: userId },
      data: {
        friends: {
          connect: {
            id: friendId,
          },
        },
      },
    });

    await prisma.user.update({
      where: { id: friendId },
      data: {
        friendOf: {
          connect: {
            id: userId,
          },
        },
      },
    });

    revalidatePath("/friends");
  } catch (error: any) {
    throw new Error(`Failed to add friend: ${error.message}`);
  }
}

export async function removeFriend(
  userId: string,
  friendId: string
): Promise<void> {
  try {
    await prisma.user.update({
      where: { id: userId },
      data: {
        friends: {
          disconnect: {
            id: friendId,
          },
        },
        friendOf: {
          disconnect: {
            id: friendId,
          },
        },
      },
    });

    await prisma.user.update({
      where: { id: friendId },
      data: {
        friends: {
          disconnect: {
            id: userId,
          },
        },
        friendOf: {
          disconnect: {
            id: userId,
          },
        },
      },
    });

    revalidatePath("/friends");
  } catch (error: any) {
    throw new Error(`Failed to remove friend: ${error.message}`);
  }
}
