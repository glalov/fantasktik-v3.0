"use server";

import { Group, Task, TaskPriority, TaskStatus, User } from "@prisma/client";
import prisma from "@/lib/prismadb";
import { revalidatePath } from "next/cache";

type CreateTaskParams = {
  name: string;
  groupID: string;
  creatorID: string;
  notes?: string;
  status?: TaskStatus;
  priority?: TaskPriority;
  rating?: number;
  reminder?: Date;
  startDate?: Date;
  dueDate?: Date;
  assignedToID?: string;
};

export async function createTask({
  name,
  groupID,
  creatorID,
  notes,
  status,
  priority,
  rating,
  reminder,
  startDate,
  dueDate,
  assignedToID,
}: CreateTaskParams): Promise<Task> {
  try {
    type TaskData = {
      name: string;
      group: {
        connect: {
          id: string;
        };
      };
      creator: {
        connect: {
          id: string;
        };
      };
      notes?: string;
      status?: TaskStatus;
      priority?: TaskPriority;
      rating?: number;
      reminder?: Date;
      startDate?: Date;
      dueDate?: Date;
      assignedTo?: {
        connect: {
          id: string | undefined;
        };
      };
    };

    const taskData: TaskData = {
      name,
      notes,
      status,
      priority,
      rating,
      reminder,
      startDate,
      dueDate,
      assignedTo: {
        connect: {
          id: assignedToID,
        },
      },
      creator: {
        connect: {
          id: creatorID,
        },
      },
      group: {
        connect: {
          id: groupID,
        },
      },
    };

    if (!assignedToID) {
      delete taskData.assignedTo;
    }

    const task = await prisma.task.create({
      data: taskData,
    });

    revalidatePath("/tasks");
    revalidatePath(`/groups/${groupID}`);
    revalidatePath(`/groups/${groupID}/edit`);
    revalidatePath(`/groups/${groupID}/calendar`);

    return task;
  } catch (error: any) {
    throw new Error(`Failed to create task: ${error.message}`);
  }
}

export type FullTaskData = Task & {
  creator: User;
  assignedTo: User | null;
  group: Group;
};

export async function getTaskById(id: string): Promise<FullTaskData | null> {
  try {
    const task = await prisma.task.findUnique({
      where: {
        id,
      },
      include: {
        creator: true,
        assignedTo: true,
        group: true,
      },
    });

    return task;
  } catch (error: any) {
    throw new Error(`Failed to get task: ${error.message}`);
  }
}

export async function updateTaskStatus(id: string, status: TaskStatus) {
  try {
    const task = await prisma.task.update({
      where: {
        id,
      },
      data: {
        status,
      },
    });

    revalidatePath("/tasks");
    revalidatePath(`/tasks/${task.id}`);
    revalidatePath(`/tasks/${task.id}/edit`);
    revalidatePath(`/groups/${task.groupID}`);
    revalidatePath(`/groups/${task.groupID}/edit`);
    revalidatePath(`/groups/${task.groupID}/calendar`);

    return task;
  } catch (error: any) {
    throw new Error(`Failed to update task status: ${error.message}`);
  }
}

export async function updateTaskPriority(id: string, priority: TaskPriority) {
  try {
    const task = await prisma.task.update({
      where: {
        id,
      },
      data: {
        priority,
      },
    });

    revalidatePath("/tasks");
    revalidatePath(`/tasks/${task.id}`);
    revalidatePath(`/tasks/${task.id}/edit`);
    revalidatePath(`/groups/${task.groupID}`);
    revalidatePath(`/groups/${task.groupID}/edit`);
    revalidatePath(`/groups/${task.groupID}/calendar`);

    return task;
  } catch (error: any) {
    throw new Error(`Failed to update task priority: ${error.message}`);
  }
}

export async function deleteTask(id: string): Promise<void> {
  try {
    const task = await prisma.task.delete({
      where: {
        id,
      },
    });

    revalidatePath("/tasks");
    revalidatePath(`/groups/${task.groupID}`);
    revalidatePath(`/groups/${task.groupID}/edit`);
    revalidatePath(`/groups/${task.groupID}/calendar`);
  } catch (error: any) {
    throw new Error(`Failed to delete task: ${error.message}`);
  }
}

type UpdateTaskParams = {
  taskId: string;
  name: string;
  groupID: string;
  notes?: string;
  status?: TaskStatus;
  priority?: TaskPriority;
  rating?: number;
  reminder?: Date;
  startDate?: Date;
  dueDate?: Date;
  assignedToID?: string;
};

export async function updateTask({
  taskId,
  name,
  groupID,
  notes,
  status,
  priority,
  rating,
  reminder,
  startDate,
  dueDate,
  assignedToID,
}: UpdateTaskParams): Promise<Task> {
  try {
    const task = await prisma.task.update({
      where: {
        id: taskId,
      },
      data: {
        name,
        notes,
        status,
        priority,
        rating,
        reminder,
        startDate,
        dueDate,
        assignedTo: assignedToID
          ? {
              connect: {
                id: assignedToID,
              },
            }
          : { disconnect: true },
        group: {
          connect: {
            id: groupID,
          },
        },
      },
    });

    revalidatePath("/tasks");
    revalidatePath(`/tasks/${task.id}`);
    revalidatePath(`/tasks/${task.id}/edit`);
    revalidatePath(`/groups/${task.groupID}`);
    revalidatePath(`/groups/${task.groupID}/edit`);
    revalidatePath(`/groups/${task.groupID}/calendar`);

    return task;
  } catch (error: any) {
    throw new Error(`Failed to update task: ${error.message}`);
  }
}

export async function unassignTask(id: string): Promise<void> {
  try {
    const task = await prisma.task.update({
      where: {
        id,
      },
      data: {
        assignedTo: {
          disconnect: true,
        },
      },
    });

    revalidatePath("/tasks");
    revalidatePath(`/tasks/${task.id}`);
    revalidatePath(`/tasks/${task.id}/edit`);
  } catch (error: any) {
    throw new Error(`Failed to unassign task: ${error.message}`);
  }
}

export async function checkUniqueName(
  name: string,
  groupID: string,
  taskId?: string
): Promise<boolean> {
  try {
    const task = await prisma.task.findFirst({
      where: {
        name,
        groupID,
      },
    });

    return task === null || (taskId !== undefined && taskId === task.id);
  } catch (error: any) {
    throw new Error(`Failed to check unique task name: ${error.message}`);
  }
}
