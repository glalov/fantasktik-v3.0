import * as z from "zod";

export const CreateGroupValidation = z.object({
  name: z.string().min(3, { message: "Minimum 3 characters." }),
  description: z.string().optional(),
});
