import { TaskPriority, TaskStatus } from "@prisma/client";
import { DateRange } from "react-day-picker";
import * as z from "zod";

export const CreateTaskValidation = z.object({
  name: z.string().min(3, { message: "Minimum 3 characters." }),
  notes: z.string().optional(),
  groupID: z.string().min(1, { message: "Please select a group." }),
  status: z.nativeEnum(TaskStatus).optional(),
  priority: z.nativeEnum(TaskPriority).optional(),
  rating: z.number().optional(),
  reminder: z.date().optional(),
  dateRange: z.custom<DateRange>().optional(),
  assignedToID: z.string().optional(),
});
