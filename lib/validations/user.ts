import * as z from "zod";

export const RegisterUserValidation = z.object({
  email: z.string().email({ message: "Invalid email address" }),
  password: z
    .string()
    .min(3, { message: "Minimum 3 characters." })
    .max(10, { message: "Maximum 10 characters." }),
  // .regex(/^[a-zA-Z0-9_.-!@#$%^&*]*$/, {
  //   message: "Should include only letters, numbers or special characters.",
  // }),
});

export const LoginUserValidation = z.object({
  email: z.string().email({ message: "Invalid email address" }),
  password: z.string(),
});

export const UpdateUserValidation = z.object({
  username: z.string().min(3, { message: "Minimum 3 characters." }),
  name: z.string().min(3, { message: "Minimum 3 characters." }),
  image: z.string(),
});
