export { default } from "next-auth/middleware";

export const config = {
  matcher: [
    "/dashboard",
    "/",
    "/add-group",
    "/groups",
    "/groups/[id]",
    "/groups/[id]/edit",
    "/groups/[id]/calendar",
    "/add-task",
    "/tasks",
    "/tasks/[id]",
    "/tasks/[id]/edit",
    "/profile",
    "/profile/edit",
    "/calendar",
    "/friends",
  ],
};
