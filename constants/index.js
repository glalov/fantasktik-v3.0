export const sidebarLinks = [
  {
    imgURL: "/assets/task_add_icon.svg",
    route: "/add-task",
    label: "Add Task",
  },
  {
    imgURL: "/assets/check_checklist_list_long_menu_icon.svg",
    route: "/tasks",
    label: "View Tasks",
  },
  {
    imgURL: "/assets/folder_plus_icon.svg",
    route: "/add-group",
    label: "Add Group",
  },
  {
    imgURL: "/assets/folders_icon.svg",
    route: "/groups",
    label: "View Groups",
  },
  {
    imgURL: "/assets/calendar.svg",
    route: "/calendar",
    label: "Calendar",
  },
  {
    imgURL: "/assets/cheerful_happy_love_hug_friends_icon.svg",
    route: "/friends",
    label: "Friend List",
  },
];

export const taskStatuses = [
  {
    value: "NOT_STARTED",
    label: "Not Started",
    color: "#d2d4d6",
  },
  {
    value: "IN_PROGRESS",
    label: "In Progress",
    color: "#dbfc7e",
  },
  {
    value: "COMPLETED",
    label: "Completed",
    color: "#98fa82",
  },
  {
    value: "PAUSED",
    label: "Paused",
    color: "#ee7efc",
  },
  {
    value: "CANCELLED",
    label: "Cancelled",
    color: "#ff7878",
  },
];

export const taskPriorities = [
  {
    value: "DEFAULT",
    label: "Default",
    color: "#d2d4d6",
  },
  {
    value: "LOW",
    label: "Low",
    color: "#7ef4fc",
  },
  {
    value: "MEDIUM",
    label: "Medium",
    color: "#f0fc7e",
  },
  {
    value: "HIGH",
    label: "High",
    color: "#ff7878",
  },
  {
    value: "CRITICAL",
    label: "Critical",
    color: "#fa1414",
  },
];

export const taskFacetedFilters = [
  {
    column: "status",
    title: "Status",
    options: taskStatuses,
  },
  {
    column: "priority",
    title: "Priority",
    options: taskPriorities,
  },
];

export const paginationSizes = [10, 20, 30, 40, 50];